<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class Home extends Controller
{
    public function index() {
        $data = [
            'title' => 'Dashboard',
            'session' => Session::get('ownerId')
        ];

        return view('backoffice.content.dashboard', $data);
    }
}

<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class Kost extends Controller
{
    public function index() {
        //, COUNT('. DB::table('kost_room')->where('kost_id', 'kost.id')->where('sewa', '1')->toSql() .') as total_rented, COUNT('. DB::table('kost_room')->where('kost_id', 'kost.id')->where('sewa', '0')->toSql() .') as total_vacant
        $kost = DB::table('kost')
            ->whereIn('status', ['0', '1'])
            ->where('owner_id', Session::get('ownerId'))
            ->get();

        $new = array();
        foreach ($kost as $loop) {
            $row = (object) [
                'id' => $loop->id,
                'owner_id' => $loop->owner_id,
                'name' => $loop->name,
                'address' => $loop->address,
                'picture' => $loop->picture,
                'status' => $loop->status,
                'total_rooms' => DB::table('kost_room')->where('kost_id', $loop->id)->where('status', '1')->get()->count(),
                'total_rented' => DB::table('kost_room')->where('kost_id', $loop->id)->where('status', '1')->where('sewa', '1')->get()->count(),
                'total_vacant' => DB::table('kost_room')->where('kost_id', $loop->id)->where('status', '1')->where('sewa', '0')->get()->count(),
            ];

            array_push($new, $row);
        }

        $data = [
            'title' => 'Dashboard Kost',
            'kosts' => $new,
        ];

        return view ('backoffice.content.kost.dashboard', $data);
    }

    public function saveKost(Request $request) {
        $request->validate([
            'imgInp' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imgName = str_replace(' ', '', Session::get('ownerUID'))."-".str_replace(' ', '', $request->nameInp).time().'.'.$request->imgInp->extension();
        $request->imgInp->move(public_path('admin/kost'), $imgName);

        DB::table('kost')->insert([
            'owner_id' => Session::get('ownerId'),
            'name' => $request->nameInp,
            'address' => $request->addressInp,
            'picture' => $imgName,
            'status' => $request->statusInp
        ]);

        return response()->json('success');
    }

    public function updateKost(Request $request) {
        $request->validate([
            'imgInp' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if (isset($request->imgInp)) {
            $imgName = str_replace(' ', '', Session::get('ownerUID'))."-".str_replace(' ', '', $request->nameInp).time().'.'.$request->imgInp->extension();
            $request->imgInp->move(public_path('admin/kost'), $imgName);
        } else {
            $imgName = DB::table('kost')->where('id', $request->id)->first();
            $imgName = $imgName->picture;
        }

        DB::table('kost')
        ->where('id', $request->id)
        ->update([
            'owner_id' => Session::get('ownerId'),
            'name' => $request->nameInp,
            'address' => $request->addressInp,
            'picture' => $imgName,
            'status' => $request->statusInp
        ]);

        return response()->json('success');
    }

    public function details($kost_id) {
        $kost = DB::table('kost')->where('id', $kost_id)->first();
        $rooms = DB::table('kost_room')
                ->join('kost', 'kost.id', '=', 'kost_room.kost_id')
                ->selectRaw('kost_room.*, kost.name as kost_name')
                ->where('kost_id', $kost_id)
                ->whereIn('kost_room.status', ['0', '1'])
                ->orderByRaw("FIELD(kost_room.status , '1', '0') ASC")
                ->get();    

        $data = [
            'title' => $kost->name,
            'kost' => $kost,
            'rooms' => $rooms
        ];

        return view('backoffice.content.kost.rooms', $data);
    }

    public function saveRoom (Request $request) {
        DB::table('kost_room')
        ->insert([
            'kost_id' => $request->kost_id,
            'name' => strtoupper($request->nameAdd),
            'status' => $request->statusAdd,
        ]);

        return response()->json('success');
    }

    public function updateRoom (Request $request) {
        DB::table('kost_room')
        ->where('id', $request->idUpd)
        ->update([
            'name' => $request->nameUpd,
            'status' => $request->statusUpd,
        ]);

        return response()->json('success');
    }

    public function viewPictures($kost_room) {
        $pictures = DB::table('kost_room_picture')->where('kost_room_id', $kost_room)->get();
        
        $data = "";
        foreach ($pictures as $loop) {
            $data .= '<div class="col-md-3">
                    <img class="img-responsive" src="'. url('admin/kost/room').'/'.$loop->picture .'" alt="your image" />
                    <button type="button" class="btn btn-danger btn-block" onclick="deletePictures(`'. $loop->id .'`, `'. $kost_room .'`)"><i class="fa fa-trash"></i> Hapus</button>
                </div>';
        }
        
        return $data;
    }

    public function deletePictures($picture_id) {
        DB::table('kost_room_picture')->where('id', $picture_id)->delete();

        return 'success';
    }

    public function savePictures(Request $request) {
        $request->validate([
            'pictAdd' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $room = DB::table('kost_room')->where('id', $request->pictRoomId)->first();

        if (isset($request->pictAdd)) {
            $imgName = str_replace(' ', '', $room->name)."-".time().'.'.$request->pictAdd->extension();
            $request->pictAdd->move(public_path('admin/kost/room'), $imgName);
        }

        DB::table('kost_room_picture')->insert([
            'kost_room_id' => $request->pictRoomId,
            'picture' => $imgName
        ]);

        return response()->json('success');
    }
}

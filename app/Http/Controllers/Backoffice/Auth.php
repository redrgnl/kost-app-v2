<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class Auth extends Controller
{
    public function bypass() {
        $owner = DB::table('owner')->where('id', '1')->first();

        Session::put('ownerId', $owner->id);
        Session::put('ownerName', $owner->name);
        Session::put('ownerUID', $owner->username);
        Session::put('login', TRUE);

        return redirect()->route('backoffice.dashboard');
    }
}

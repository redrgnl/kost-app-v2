@extends('backoffice.index')

@section('content')

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <a href="#" data-toggle="modal" data-target="#modal-add-kost">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ count($kosts) }} <i class="fa fa-home"></i></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-plus"></i>
                </div>
                <div class="small-box-footer">
                    Tambahkan Kos Baru <i class="fa fa-arrow-circle-right"></i>
                </div>
            </div>
        </a>
    </div>
</div>

<h3>Data Kost</h3>

<div class="row">
    @foreach($kosts as $kost)
    <div class="col-md-4">
        <a href="{{ route('owner.kost.details', ['kost_id' => $kost->id]) }}" style="text-decoration: none!important; color: black;">
            <div class="box box-widget widget-user">
                <div class="widget-user-header bg-black" style="background: url('{{ url('admin/kost').'/'.$kost->picture }}') center center;">
                    @if ($kost->status == 1)
                        <span class="label label-success">Aktif</span>
                    @else
                        <span class="label label-danger">Non-Aktif</span>
                    @endif
                    <h3 class="widget-user-username">
                        {{ $kost->name }}
                    </h3> 
                    
                    <h5 class="widget-user-desc">{{ $kost->address }}</h5>
                </div>
                <div class="box-footer" >
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $kost->total_rooms }}</h5>
                                <span class="description-text">Kamar</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $kost->total_rented }}</h5>
                                <span class="description-text">Tersewa</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">{{ $kost->total_vacant }}</h5>
                                <span class="description-text">Kosong</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>

<div class="modal fade" id="modal-add-kost">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Kost Baru</h4>
            </div>
            <form action="{{ route('owner.kost.save') }}" method="POST" id="insert-new-kost" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nameInp">Nama Kost</label>
                                    <input type="text" class="form-control" id="nameInp" name="nameInp" placeholder="Masukkan Nama" maxlength="100">
                                </div>

                                <div class="form-group">
                                    <label for="addressInp">Alamat</label>
                                    <input type="text" class="form-control" id="addressInp" name="addressInp" placeholder="Masukkan Alamat">
                                </div>

                                <div class="form-group">
                                    <label for="imgInp">Foto</label>
                                    <input type="file" id="imgInp" name="imgInp" accept="image/*" style="display: none;">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="imgPrv" name="imgPrv" class="img-responsive" src="{{ url('admin/dist/img/sample-img.png') }}" alt="your image" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="statusInp">Status</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <input type="checkbox" name="statusInp" id="statusInp" data-toggle="toggle" data-on="Aktif" data-off="Non-Aktif" data-onstyle="info" data-offstyle="danger" data-width="100" data-height="35">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
        if (file) {
            imgPrv.src = URL.createObjectURL(file)
        }
    }

    $('#imgPrv').on('click', function () {
        $('#imgInp').trigger('click')
    })

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#insert-new-kost').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        
        (document.getElementById('statusInp').checked) ? formData.set('statusInp', '1') : formData.set('statusInp', '0');

        $.ajax({
            type:'POST',
            url: "{{ route('owner.kost.save') }}",
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#imgPrv').attr('src', "{{ url('admin/dist/img/sample-img.png') }}")
                    $('#modal-add-kost').modal('toggle');

                    window.location.href = window.location.href;
                }
            },
            error: function(response){
                alert('Error');
            }
        });
    });

</script>
@endsection
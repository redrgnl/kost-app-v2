@extends('backoffice.index')

@section('content')
<div class="row">
    <div class="col-md-12">
            <div class="box box-widget widget-user">
                <div class="widget-user-header bg-black" style="background: url('{{ url('admin/kost').'/'.$kost->picture }}') center center;">
                    <div class="row">
                        <div class="col-md-3">
                            @if ($kost->status == 1)
                                <span class="label label-success">Aktif</span>
                            @else
                                <span class="label label-danger">Non-Aktif</span>
                            @endif
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="widget-user-username">
                                {{ $kost->name }}
                            </h3> 
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-3">
                            <h5 class="widget-user-desc">{{ $kost->address }}</h5>
                        </div>
                    </div>            
                </div>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a href="#" data-toggle="modal" data-target="#modal-edit-details">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-gears"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Detail Kost</span>
                    <span class="info-box-number"></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                    <i class="fa fa-arrow-circle-right"></i> Edit Detail Kost
                    </span>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <a href="#" data-toggle="modal" data-target="#modal-add-room">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tambah</span>
                    <span class="info-box-number"></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                    <i class="fa fa-arrow-circle-right"></i> Tambah Kamar Baru
                    </span>
                </div>
            </div>
        </a>
    </div>
</div>

<h3>Kamar</h3>

<div class="row">
    @foreach ($rooms as $room)
    <div class="col-md-3" id="room-data" data-room="{{ $room->id.'%'.$room->name.'%'.$room->status }}">
        <div class="box box-widget widget-user-2">
            <a href="#" data-toggle="modal" data-target="#modal-edit-room">
                <div class="widget-user-header bg-aqua">
                    <div class="info-box bg-aqua" style="box-shadow: none !important">
                        <span class="info-box-icon"><i class="fa fa-home"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{ $room->kost_name }}</span>
                            <span class="info-box-number">{{ $room->name }}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description"></span>
                        </div>
                    </div>
                </div>
            </a>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li>
                        <a> Status
                            @if ($room->status == 1)
                            <span class="pull-right badge bg-green">Aktif</span>
                            @else
                            <span class="pull-right badge bg-red">Non-Aktif</span>
                            @endif

                            @if ($room->sewa == 1)
                            <span class="pull-right badge bg-blue" style="margin-right: 3px">Tersewa</span>
                            @else
                            <span class="pull-right badge bg-orange" style="margin-right: 3px">Kosong</span>
                            @endif
                        </a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#modal-crud-picture" onclick="viewPictures('{{$room->id}}')"> Foto
                            <span class="pull-right badge bg-blue">
                                <i class="fa fa-fw fa-photo"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a style="text-decoration: none"> 
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                            <p class="badge bg-green" style="margin-right: 3px">Aktif</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach
</div>

<!-- modals -->
<!-- modal edit data kost -->
<div class="modal fade" id="modal-edit-details">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Detail Kost</h4>
            </div>
            <form action="{{ route('owner.kost.update') }}" method="POST" id="update-detail-kost" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nameInp">Nama Kost</label>
                                    <input type="text" class="form-control" id="nameInp" name="nameInp" placeholder="Masukkan Nama" maxlength="100" value="{{ $kost->name }}">
                                </div>

                                <div class="form-group">
                                    <label for="addressInp">Alamat</label>
                                    <input type="text" class="form-control" id="addressInp" name="addressInp" placeholder="Masukkan Alamat" value="{{ $kost->address }}">
                                </div>

                                <div class="form-group">
                                    <label for="imgInp">Foto</label>
                                    <input type="file" id="imgInp" name="imgInp" accept="image/*" style="display: none;">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="imgPrv" name="imgPrv" class="img-responsive" src="{{ url('admin/kost').'/'.$kost->picture }}" alt="your image" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="statusInp">Status</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <input type="checkbox" <?php if ($kost->status == 1) { echo "checked"; } else { echo ""; }; ?> name="statusInp" id="statusInp" data-toggle="toggle" data-on="Aktif" data-off="Non-Aktif" data-onstyle="info" data-offstyle="danger" data-width="100" data-height="35">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal tambah data kamar -->
<div class="modal fade" id="modal-add-room">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Kamar Baru</h4>
            </div>
            <form action="{{ route('owner.kost.save.room') }}" method="POST" id="add-new-room" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nameAdd">Nama Kamar</label>
                                    <input type="text" class="form-control" id="nameAdd" name="nameAdd" placeholder="Masukkan Nama" maxlength="100">
                                </div>

                                <div class="form-group">
                                    <label for="statusAdd">Status</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <input type="checkbox" checked name="statusAdd" id="statusAdd" data-toggle="toggle" data-on="Aktif" data-off="Non-Aktif" data-onstyle="info" data-offstyle="danger" data-width="100" data-height="35">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit data kamar -->
<div class="modal fade" id="modal-edit-room">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Data Kamar</h4>
            </div>
            <form action="{{ route('owner.kost.update.room') }}" method="POST" id="update-room-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="box-body">
                                <input type="hidden" id="idUpd" name="idUpd">

                                <div class="form-group">
                                    <label for="nameUpd">Nama Kamar</label>
                                    <input type="text" class="form-control" id="nameUpd" name="nameUpd" placeholder="Masukkan Nama" maxlength="100">
                                </div>

                                <div class="form-group">
                                    <label for="statusUpd">Status</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <input type="checkbox" name="statusUpd" id="statusUpd" data-toggle="toggle" data-on="Aktif" data-off="Non-Aktif" data-onstyle="info" data-offstyle="danger" data-width="100" data-height="35">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal tambah / edit foto -->
<div class="modal fade" id="modal-crud-picture">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detail Foto Ruangan</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 20px">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-block btn-info" id="pict-add"><i class="fa fa-plus"></i> Tambah</button>
                        <form action="{{ route('owner.kost.save.pictures') }}" method="POST" id="saveRoomPictures" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="pictRoomId" name="pictRoomId">
                            <input type="file" id="pictAdd" name="pictAdd" accept="image/*" style="display: none;">
                        </form>
                    </div>
                </div>
                <div class="row" id="view-pictures"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
        if (file) {
            imgPrv.src = URL.createObjectURL(file)
        }
    }

    $('#imgPrv').on('click', function () {
        $('#imgInp').trigger('click')
    })

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#update-detail-kost').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        
        (document.getElementById('statusInp').checked) ? formData.set('statusInp', '1') : formData.set('statusInp', '0');
        formData.append('id', '{{ $kost->id }}')

        $.ajax({
            type:'POST',
            url: "{{ route('owner.kost.update') }}",
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#imgPrv').attr('src', "{{ url('admin/dist/img/sample-img.png') }}")
                    $('#modal-edit-details').modal('toggle');

                    window.location.href = window.location.href;
                }
            },
            error: function(response){
                alert('Error');
            }
        });
    });

    $('#add-new-room').submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        
        (document.getElementById('statusAdd').checked) ? formData.set('statusAdd', '1') : formData.set('statusAdd', '0');
        formData.append('kost_id', '{{ $kost->id }}')

        $.ajax({
            type:'POST',
            url: "{{ route('owner.kost.save.room') }}",
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#modal-add-room').modal('toggle');

                    window.location.href = window.location.href;
                }
            },
            error: function(response){
                alert('Error');
            }
        });
    })

    $('#room-data').on('click', function() {
        var data = $('#room-data').data('room').toString().split('%');
        $('#idUpd').val(data[0])
        $('#nameUpd').val(data[1])

        if (data[2] == 1) {
            $('#statusUpd').bootstrapToggle('on')
        } else {
            $('#statusUpd').bootstrapToggle('off')
        }
    })

    $('#update-room-data').submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        
        (document.getElementById('statusUpd').checked) ? formData.set('statusUpd', '1') : formData.set('statusUpd', '0');

        $.ajax({
            type:'POST',
            url: "{{ route('owner.kost.update.room') }}",
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    this.reset();
                    $('#modal-edit-room').modal('toggle');

                    window.location.href = window.location.href;
                }
            },
            error: function(response){
                alert('Error');
            }
        });
    });

    function viewPictures(kost_room) {
        var url = "{{ route('owner.kost.view.pictures', ['kost_room' => ':id']) }}";
        url = url.replace(':id', kost_room);

        $('#pictRoomId').val(kost_room)

        $.ajax({
            type: "GET",
            url: url,
            contentType: false,
            processData: false,
            success: (response) => {
                $('#view-pictures').html('')
                $('#view-pictures').html(response)
            },
            error: function(response){
                alert('Error');
            }
        });
    }

    function deletePictures(picture_id, kost_room) {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus foto?',
            showConfirmButton: false,
            showDenyButton: true,
            denyButtonText: `Hapus`,
        }).then((result) => {
            if (result.isDenied) {
                var url = "{{ route('owner.kost.delete.pictures', ['picture_id' => ':id']) }}";
                url = url.replace(':id', picture_id);

                $.ajax({
                    type: "GET",
                    url: url,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        Swal.fire('Proses Berhasil!!', '', 'success')
                        viewPictures(kost_room);
                    },
                    error: function(response){
                        alert('Error');
                    }
                });
            }
        })
    }

    $('#pict-add').on('click', function () {
        $('#pictAdd').trigger('click')
    });

    pictAdd.onchange = evt => {
        var formData = new FormData(saveRoomPictures);
        $.ajax({
            type:'POST',
            url: "{{ route('owner.kost.save.pictures') }}",
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response) {
                    viewPictures($('#pictRoomId').val())
                    saveRoomPictures.reset();
                }
            },
            error: function(response){
                alert('Error');
            }
        });
    }

    
</script>
@endsection
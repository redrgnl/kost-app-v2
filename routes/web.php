<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backoffice\Auth;
use App\Http\Controllers\Backoffice\Home;
use App\Http\Controllers\Backoffice\Kost;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [Home::class, 'index'])->name('base.page');
Route::get('/bypass', [Auth::class, 'bypass']);

Route::group(['prefix' => 'backoffice'], function () {
    Route::get('/', [Home::class, 'index'])->name('backoffice.dashboard');

    Route::group(['prefix' => 'owner'], function () {
        // kost
        Route::group(['prefix' => 'kost'], function () {
            Route::get('/', [Kost::class, 'index'])->name('owner.kost.dashboard');
            Route::get('/{kost_id}/details', [Kost::class, 'details'])->name('owner.kost.details');

            // Process
            Route::post('/save-kost', [Kost::class, 'saveKost'])->name('owner.kost.save');
            Route::post('/update-kost', [Kost::class, 'updateKost'])->name('owner.kost.update');

            Route::post('/save-room', [Kost::class, 'saveRoom'])->name('owner.kost.save.room');
            Route::post('/update-room', [Kost::class, 'updateRoom'])->name('owner.kost.update.room');
            // End Process

            //room
            Route::get('/{kost_room}/room-pictures', [Kost::class, 'viewPictures'])->name('owner.kost.view.pictures');
            Route::get('/{picture_id}/delete-pictures', [Kost::class, 'deletePictures'])->name('owner.kost.delete.pictures');
            Route::post('/save-room-pictures', [Kost::class, 'savePictures'])->name('owner.kost.save.pictures');
            // end room
        });
    });
});
